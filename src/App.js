import logo from './logo.svg';
import './App.css';

function App(props) {
  const element = (
  <div className="App">Hello {props.name}
  <h2>It is {new Date().toLocaleTimeString()}.</h2>
  </div>
  )
  return (
   element
  );
}

export default App;
